package Zadanie02;
import java.rmi.MarshalException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
public class Print {
	 void print(String s) {
	        if (s == null) {
	            throw new NullPointerException("Exception: s is null!");
	        }
	        System.out.println("Inside method print: " + s);
	    }
	 public String input() throws MarshalException {
		 //������������� � ������� throws,
		// ��� ����� ����� ��������� ���������� MarshalException
		      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		    String s = null;
		//� ���� try ��������� ���, � ������� ����� ��������� ����������, � ������
		// ������ ���������� ��� ������������, ��� ����� readLine() ������
		// BufferedReader ����� ��������� ���������� �����/������
		    try {
		        s = reader.readLine();
		// � ����  catch ��������� ��� �� ��������� ���������� IOException
		    } catch (IOException e) {
		        System.out.println(e.getMessage());
		// � ����� finally ��������� ����� ������
		    } finally {
		// ��� �������� ������ ���� �������� ����������, ��������, ���� �� �� ��� ������, ������� ������������ ��� � ���� try
		        try {
		            reader.close();
		// ����� ��������� ���������� ��� �������� ������ ������
		        } catch (IOException e) {
		            System.out.println(e.getMessage());
		        }
		    }

		    if (s.equals("")) {
		// �� ������, ��� ������ ������ ����� �������� � ���������� ������ ����� ���������, ��������, �� ���������� ����� ������ ��� ���� �������� ����� substring(1,2), ������� �� ��������� �������� ���������� ��������� � ���������� ������ ���� ���������� MyException � ������� throw
		        throw new MarshalException("String can not be empty!");
		    }
		    return s;
		}
}
