package Zadanie02;

public abstract class Transport {
	protected double time;
	protected double price;
	protected int NumberOfPerson;
	public abstract double getTime();
	public abstract double getPrice();
	public abstract int getNumberOfPerson();
	public abstract void  setTime(double time);
	public abstract void  setPrice(double price);
	public abstract void  setNumberOfPerson(int NumberOfPerson);
}
