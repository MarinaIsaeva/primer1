package Zadanie02;

public class Plane extends Transport{
	@Override
	public double getTime(){
		return time;
	}
	@Override
	public double getPrice(){
		return price;
	}
	@Override
	public int getNumberOfPerson(){
		return NumberOfPerson;
	}
	@Override
	public void setTime(double time){
		this.time=time;
	}
	@Override
	public void setPrice(double price){
		this.price=price;
	}
	@Override
	public void setNumberOfPerson(int NumberOfPerson){
		this.NumberOfPerson=NumberOfPerson;
}
}
