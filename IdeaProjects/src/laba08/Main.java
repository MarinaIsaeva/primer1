package laba08;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
public class Main {

    public static void main(String[] args) {
// 1-e задание
        int [] values = { 1, 4, 9, 16 };
        Stream.of(Arrays.toString(values)).forEach(System.out::println);
        //IntStream values2= Arrays.stream(values);
      //  Stream<int[]> values1=Stream.of(values);
       // System.out.println(Arrays.toString(values));
// 2-e задание
        List<Person> persons =
                Arrays.asList(
                        new Person("Andrew", 20),
                        new Person("Igor", 23),
                        new Person("Ira", 23),
                        new Person("Vitia", 12));
        /*Reduce метод принимает функцию аккумулятора BinaryOperator.
        Это на самом деле BiFunction, когда оба операнда имеют один и тот же тип, в этом случае Person.
         BiFunctions похожи на Function, но принимает два аргумента.
         Пример функции сравнивает людей по возрасту и возвращает самого старшего
          */

        persons
                .stream()
                .reduce((p1, p2) -> p1.age > p2.age ? p1 : p2)
                .ifPresent(System.out::println);    // Ira
        /*Второй Reduce метод принимает идентифицирующее значение и BinaryOperator.
        Этот метод может быть использован для «создания» нового человека
        с агрегированным имен и возрастом других человек в потоке:
         */
        Person result =
                persons
                        .stream()
                        .reduce(new Person("", 0), (p1, p2) -> {
                            p1.age += p2.age;
                            p1.name += p2.name;
                            return p1;
                        });

        System.out.format("name=%s; age=%s", result.name, result.age);
// name=AndrewIgorIraVitia; age=78
/*Третий Reduce  метод принимает три параметра:
 значение идентификатора,BiFunction аккумулятор и объединитель функции типа BinaryOperator.
 Поскольку идентифицирующее значение не ограничивает тип Person,
  мы можем использовать это сокращение для определения суммы возрасте от всех лиц:
  */
        Integer ageSum = persons
                .stream()
                .reduce(0, (sum, p) -> sum += p.age, (sum1, sum2) -> sum1 + sum2);

        System.out.println("\n" +ageSum);  // 78
        /*аккумулирующая функция делает всю работу.
        Сначала вызывается инициализирующая значение 0 и первый человек Андрей.
         В следующих трех вызовах «sum» увеличивается возраст до суммарного 78
         */
        Integer ageSum1 = persons
                .stream()
                .reduce(0,
                        (sum, p) -> {
                            System.out.format("accumulator: sum=%s; person=%s\n", sum, p);
                            return sum += p.age;
                        },
                        (sum1, sum2) -> {
                            System.out.format("combiner: sum1=%s; sum2=%s\n", sum1, sum2);
                            return sum1 + sum2;
                        });

//accumulator: sum=0; person=Andrew
//accumulator: sum=20; person=Igor
//accumulator: sum=43; person=Ira
//accumulator: sum=66; person=Vitia

    }
}
