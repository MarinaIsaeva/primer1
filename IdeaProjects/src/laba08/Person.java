package laba08;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Person {
    String name;
    int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return name;
    }
    //2
   /* public static <T> String asString(List<T> list) {
        return StreamSupport.stream(list)
                .map(Object::toString)
                .collect(Collectors.joining("\n"));
    }  */
}
