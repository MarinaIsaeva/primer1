package marina06;
import java.lang.Object;
import java.util.Arrays;
import java.util.ArrayList;
import java.lang.System;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.lang.Iterable;
import java.lang.Boolean;
import java.lang.Exception;
import java.io.EOFException;
import java.io.NotActiveException;
import java.lang.IndexOutOfBoundsException;
import java.lang.StringBuffer;

public class DynamicArray<T> {
    public T[] array;
    int size=8;
    int count;
    //1  Конструктор без параметров (создается массив емкостью 8 элементов).
    public DynamicArray()
    {
        this.array = (T[])new Object[8];
    }
    //2 Конструктор с 1 целочисленным параметром (создается массив заданной емкости).
    public DynamicArray(int length)
    {
        array =(T[]) new Object[length];
    }
    //3  Конструктор, который в качестве параметра принимает перечисление
    public DynamicArray(T[]...params)
    {
        this.array = (T[]) new Object[params.length];
    }
    //4  Метод Add, добавляющий в конец массива один элемент. При нехватке места для добавления элемента емкость массива должна расширяться в 2 раза
    public void Add(T elem)
    {
        Resize(count);
        array[count] = elem;
        count++;
    }
    private void Resize(int index)
    {
        if (size < index || size == index)
        {
            while (size < index || size == index)
            {
                NextSize();
            }

            T[] Garbage = (T[]) new Object[size];

            for (int i = 0; i < array.length; i++)
            {
                Garbage[i] = array[i];
            }

            array = Garbage;
        }
    }
    //Увеличение размера в два раза
    private void NextSize()
    {
        if (size == 0)
        {
            size = 4;
        }

        size *= 2;
    }

    public void RemoveAt(int index)
    {
        if (index >= count)
        {
            throw new IndexOutOfBoundsException();
        }
        int shiftStart = index + 1;
        if (shiftStart < count)
        {
            System.arraycopy(array, shiftStart, array, index, count - shiftStart);
        }
        count--;
    }
    //6  Метод Remove
    public boolean Remove(T item)
    {
        for (int i = 0; i < count; i++)
        {
            if (array[i].equals(item))
            {
                RemoveAt(i);
                return true;
            }
        }
        return false;
    }
    //7 Метод Insert
    public  boolean  Insert(T elem, int index)
    {
        Resize(count + 1);

        T tmp1 = array[index];
        T tmp2=null;
        T tmp3=null;

        try { tmp2 = array[index + 1]; }
        catch (Exception ex) {ex.printStackTrace(); }

        try { tmp3 = array[index + 2]; }
        catch (Exception ex) {ex.printStackTrace(); }

        int i = index;

        for (; i != array.length; i++)
        {
            array[i + 1] = tmp1;
            tmp1 = tmp2;
            tmp2 = tmp3;

            try { tmp3 = array[i + 3]; }
            catch (Exception ex) {ex.printStackTrace(); }
        }

        array[index] = elem;
        count++;
        return true;
    }
    //8 Length
    public int Length()
    {
        return array.length;
    }
    //5 Метод AddRange, добавляющий в конец массива содержимое переданного перечисления
    public void AddRange(T...args)
    {
        if(args.length+size>count){
            arrayExpansion(args.length);
        } int j=size;
        for(int i=0;i<args.length; i++){
            array[ j++]=args[i];
        }
        size+=args.length;
    }
    //9 расширение(емкость)
    private void arrayExpansion(int size){
        T[] newArray= (T[])new Object[size+count];
        for (int i=0;i<count;i++){
            newArray[i]=array[i];
        }
        count =count+size;
        array=newArray;
    }
    //9
    public int getCount(){
        return count;
    }
    public T set(int index, T element) {

        return array[index] = element;
    }
    public T get(int index) {

        if(index > size || index < 0) {

            throw new ArrayIndexOutOfBoundsException();

        }
        return array[index];
    }

}
