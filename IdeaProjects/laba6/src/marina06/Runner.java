package marina06;
import java.util.Scanner;
import java.util.Arrays;
import java.lang.Object;
import java.util.Collections;

public class Runner {
    DynamicArray array1 = new DynamicArray<Object>(10);
    DynamicArray<Integer> array = new DynamicArray();
		System.out.println(array1);
		System.out.println(array);
		array.Add(399);
		array.Add(-100);
		array.Add(444);
		array.Insert(666, 2);
		System.out.println(array);
		array.Remove(new Integer(1));
		System.out.println(array);
    //Collections.sort(array);
		System.out.println(array);
		array.AddRange(5, -100, 77);
		System.out.println(array);
		array.Remove(new Integer(666));
		System.out.println(array);
		array.set(0, 33);
		System.out.println(array);
		System.out.println(array.get(0));
}
